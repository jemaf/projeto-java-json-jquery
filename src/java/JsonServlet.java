/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author edumontandon
 */
@WebServlet(urlPatterns = {"/JsonServlet"})
public class JsonServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
      
//        define os parâmetros básicos da resposta
        response.setContentType("text/html;charset=UTF-8");
        response.setContentType("application/json");
        response.setStatus(HttpServletResponse.SC_OK);
                
        PrintWriter out = response.getWriter();

//        preenche json
        String json = "{\n"
                + "\"method\":\"" + request.getMethod() + "\",\n"
                + "\"host\":\"" + request.getRemoteHost() + "\",\n"
                + "\"url\":\"" + request.getRequestURI() + "\"\n"
                + "}";
        
//        escreve a resposta
        out.println(json);
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        define os parâmetros básicos da resposta
        response.setContentType("text/html;charset=UTF-8");
        response.setContentType("application/json");
        response.setStatus(HttpServletResponse.SC_OK);
                
        PrintWriter out = response.getWriter();

//        preenche json
        String json = "{\n"
                + "\"method\":\"" + request.getMethod() + "\",\n"
                + "\"host\":\"" + request.getRemoteHost() + "\",\n"
                + "\"url\":\"" + request.getRequestURI() + "\"\n"
                + "}";
        
//        escreve a resposta
        out.println(json);
        out.close();
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         
//        recupera o JSON enviado
        BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream()));
        String json = "";
        if(br != null){
            json = br.readLine();
        }
        
//        imprime o JSON na resposta
        System.out.println(json);
        response.setStatus(HttpServletResponse.SC_OK);
//        
        PrintWriter out = response.getWriter();
        out.println(json);
        out.close();        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
